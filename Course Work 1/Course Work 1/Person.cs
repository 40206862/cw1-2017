﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Course_Work_1
{
    public class Person
    {
        /*
     * Created by Drew Jamieson
     * Person.cs is the parent to Attendee.cs and is used to store the more general attributes of a person, such as the name
     * Last modified 13th October 2016
     */


        //Use 'set; get;' to assign names to variables and initialise properties of class person and ensure they are not empty
        //If either of them is empty, do not fill in the specific area
        //Stores the first Name
        private string attendee_firstName;
        public string firstName
        {
            get
            {
                return attendee_firstName;
            }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    throw new ArgumentException("Please ensure that the First Name box is not empty.");
                attendee_firstName = value;
                
            }
        }

        //Stores the last name
        private string attendee_lastName;
        public string lastName
        {
            get
            {
                return attendee_lastName;
            }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    throw new ArgumentException("Please ensure that the Last Name box is not empty.");
                attendee_lastName = value;
            }
        }
    }
}
