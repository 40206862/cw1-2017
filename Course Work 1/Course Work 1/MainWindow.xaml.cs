﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Course_Work_1
{
    /*
      * Created by Drew Jamieson
      * MainWindow.xaml.cs is used as the main window when starting the program, allowing the user to input all the details about
      * the object 'attendee'
      * 
      * Last modified 13th October 2016
      */
    

    public partial class MainWindow : Window
    {
        public Attendee objAttendee = new Attendee();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnSet_Click(object sender, RoutedEventArgs e)
        {

            //Check if txtFirstName is null or contains only whitespace before assigning.
            try
            {
                objAttendee.firstName = txtFirstName.Text;
            }
            catch (Exception excep)
            {
                Console.WriteLine(excep.Message);
            }



            //Check if txtSurname is empty or contains only whitespace before assigning.
            try
            {
                objAttendee.lastName = txtSurname.Text;
            }
            catch (Exception excep)
            {
                MessageBox.Show(excep.Message);
            }



            //Check that txtAttendee is not empty or whitespace as well as not containing any non-int characters
            try
            {
                objAttendee.attendeeRef = txtAttendee.Text;
            }
            catch (Exception excep)
            {
                MessageBox.Show(excep.Message);
            }



            //Check the txtConference is not left blank
            try
            {
                objAttendee.conferenceName = txtConference.Text;
            }
            catch (Exception excep)
            {
                MessageBox.Show(excep.Message);
            }



            //Check that txtRegistration type is 'full', 'student' or 'organiser'
            try
            {
                objAttendee.registrationType = txtRegistration.Text;
            }
            catch (Exception excep)
            {
                MessageBox.Show(excep.Message);
            }



            //Check that txtPaid is true or false
            try
            {
                bool result;
                if (Boolean.TryParse(txtPaid.Text, out result))                                
                    objAttendee.paid = Convert.ToBoolean(txtPaid.Text);                
                else
                    MessageBox.Show("Please ensure the paid text box is either true or false.");
                }
            catch (Exception excep)
            {
                MessageBox.Show(excep.Message);
            }

            
            //Check that txtPresenter is true or false
            try
            {
                bool result;
                if (Boolean.TryParse(txtPresenter.Text, out result))                                
                    objAttendee.presenter = Convert.ToBoolean(txtPresenter.Text);                
                else
                    MessageBox.Show("Please ensure the presenter text box is either true or false.");
            }
            catch (Exception excep)
            {
                MessageBox.Show(excep.Message);
            }


            
            //If presenter is true then check that txtPaper isn't empty
            if (objAttendee.presenter)
            {
                try
                {
                    objAttendee.paperTitle = txtPaper.Text;
                }
                catch (Exception excep)
                {
                    MessageBox.Show(excep.Message);
                }
            }
            //If presenter is false then check that txt paper is empty
            else if (!objAttendee.presenter)
            {
                if (!(string.IsNullOrWhiteSpace(txtPaper.Text)))
                    MessageBox.Show("Please ensure that the Paper Title box is empty.");
            }

            //Assign Institution Name
            objAttendee.institutionName = txtInstitute.Text;
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            //Clear the textboxes
            txtFirstName.Text = "";
            txtSurname.Text = "";
            txtAttendee.Text = "";
            txtInstitute.Text = "";
            txtConference.Text = "";
            txtRegistration.Text = "";
            txtPaid.Text = "";
            txtPresenter.Text = "";
            txtPaper.Text = "";
        }

        private void btnGet_Click(object sender, RoutedEventArgs e)
        {
            //Assign textboxes to have the data held within objAttendee
            txtFirstName.Text = objAttendee.firstName;
            txtSurname.Text = objAttendee.lastName;
            txtAttendee.Text = objAttendee.attendeeRef;
            txtInstitute.Text = objAttendee.institutionName;
            txtConference.Text = objAttendee.conferenceName;
            txtRegistration.Text = objAttendee.registrationType;
            txtPaid.Text = objAttendee.paid.ToString();
            txtPresenter.Text = objAttendee.presenter.ToString();
            txtPaper.Text = objAttendee.paperTitle;
        }


        private void btnInvoice_Click(object sender, RoutedEventArgs e)
        {
            Invoice Invoice = new Invoice(objAttendee);
            
            Invoice.Show();
        }

        private void btnCertificate_Click(object sender, RoutedEventArgs e)
        {
            Certificate Certificate = new Certificate(objAttendee);

            Certificate.Show();
        }

        private void txtConference_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void txtInstitute_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}

