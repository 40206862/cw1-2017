﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Course_Work_1
{
    /*
     * Created By Drew Jamieson
     * Invoice.xaml.cs is used to interact with the 'invoice' button on the main window, allowing for an invoice message to be displayed
     * 
     * Last modified 13th October 2016
     
     */
    public partial class Invoice : Window
    {
        public Invoice()
        {
            InitializeComponent();
        }

        public Invoice(Attendee objAttendee)
        {
            InitializeComponent();
            //Display invoice.
            lblInvoice.Content = "Name: " + objAttendee.firstName + " " + objAttendee.lastName + "\n" 
                + "Institution: " + objAttendee.institutionName + "\n"
                + "Conference: " + objAttendee.conferenceName + "\n"
                + "Cost to be paid: £"  + objAttendee.getCost();
        }
    }
}
