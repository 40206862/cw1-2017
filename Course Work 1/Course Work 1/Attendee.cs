﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Course_Work_1
{
    public class Attendee : Person
    {
        /*
         * Created by Drew Jamieson
         * attendee.cs used to create child object of Person, to allow the user to store the details of the attendee.
         * Temp variables used to avoid recursion within the program
         * Use the get and set methods to assign variables to properties of attendee, as well as catch any errors
         * 
         * Last modified 13th October 2016
        */

        //Stores the institution name
        private string attendee_institutionName;
        public string institutionName
        {
            get
            {
                return attendee_institutionName;
            }
            set
            {
                attendee_institutionName = value;
            }
        }

        //stores the conference name
        private string attendee_conferenceName;
        public string conferenceName
        {
            get
            {
                return attendee_conferenceName;
            }
            set
            {
                //Check that txtConference is not null or whitespace
                if (string.IsNullOrWhiteSpace(value))
                    throw new ArgumentException("Please ensure that the Conference Name box is not empty.");
                attendee_conferenceName = value;
            }
        }

        //Stores the registration type, full, student, or organiser
        private string attendee_registrationType;
        public string registrationType
        {
            get
            {
                return attendee_registrationType;
            }
            set
            {
                //Check if txtRegistration is not null, or whitespace and is either 'full','student' or 'organiser'
                if (string.IsNullOrWhiteSpace(value) && (string.Equals(value.ToLower(), "full") || string.Equals(value.ToLower(), "student") || string.Equals(value.ToLower(), "organiser")))
                    throw new ArgumentException("Please ensure that the Registration Type box is not empty.");
                attendee_registrationType = value;
            }
        }

        //Stores the paper title 
        private string attendee_paperTitle;
        public string paperTitle
        {
            get
            {
                return attendee_paperTitle;
            }
            set
            {
                if (presenter)
                {
                    //Check if txtPaper is not null
                    if (string.IsNullOrWhiteSpace(value))
                        throw new ArgumentException("Please ensure that the Paper Title box is not empty.");
                    attendee_paperTitle = value;
                }
                //If presenter is false then check that txt paper is empty
                else if (!presenter)
                {
                    if (!(string.IsNullOrWhiteSpace(value)))
                        throw new ArgumentException("Please ensure that the Paper Title box is empty.");
                }


                
            }
        }

        //Stores the attendee reference number
        private string attendee_attendeeRef;
        public string attendeeRef
        {
            get
            {
                return attendee_attendeeRef;
            }
            set
            {
                //Check if attendee reference textbox is null or whitespace
                if (!(string.IsNullOrWhiteSpace(value)))
                {
                    //Check that txtAttendee contains only numerical values
                    foreach (char character in value)
                    {
                        if (character < '0' || character > '9')
                        {
                            throw new ArgumentException("Please ensure that the yor attendee reference is a number.");
                        }
                    }
                    //Check that txtAttendee is between 40000 and 60000 inclusive
                    if (Convert.ToInt32(value) < 40000 || Convert.ToInt32(value) > 60000)
                        throw new ArgumentException("Please ensure that the attendee reference number is between 40000 and 60000 inclusively.");
                }
                //If txtAttendee is whitespace or null
                else
                    throw new ArgumentException("Please ensure that the Attendee Reference box is not empty.");
                //If txtattendee is not blank, does not contain any non numerical characters and is between 40000 and 60000
                attendee_attendeeRef = value;
            }
        }

        //Marks if attendee has paid
        private bool attendee_paid;
        public bool paid
        {
            get
            {
                return attendee_paid;
            }
            set
            {
                attendee_paid = value;
            }
        }

        //Marks if attendee is a presenter
        private bool attendee_presenter;
        public bool presenter
        {
            get
            {
                return attendee_presenter;
            }
            set
            {
                attendee_presenter = value;
            }
        }


        //Get cost method to find the cost that an attendee should pay
        public float getCost()
        {
            float cost = 0;
            //Assign the cost of attending compared to the different registration types as long as registration type doesn't equal null
            if (attendee_registrationType!=null)
            {
                if (attendee_registrationType.ToLower() == "full")
                    cost = 500;
                else if (attendee_registrationType.ToLower() == "student")
                    cost = 300;
                else if (attendee_registrationType.ToLower() == "organiser")
                    cost = 0;


                //If the attendee is a presenter then remove 10% of the cost.
                if (attendee_presenter)
                    cost = cost - (cost/10);

                //Round the cost to 2 decimal place
                Math.Round(cost, 2);
            }
            return cost;
        }
    }
}