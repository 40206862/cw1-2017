﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Course_Work_1
{
    /*
     * Created by Drew Jamieson
     * Certificate.xaml.cs is used to interact with the 'certificate button on the main window, displaying a certificate and set message
     * 
     * Last modified 13th October 2016
     */
    public partial class Certificate : Window
    {
        public Certificate()
        {
            InitializeComponent();
        }

        
        //When certificate window is generated fill the label with relevant details
        public Certificate(Attendee objAttendee)
        {
            InitializeComponent();
            lblCertificate.MaxWidth = lblCertificate.Width;
            //Check if any used parameters are null
            if (objAttendee.firstName != null && objAttendee.lastName != null && objAttendee.conferenceName != null && objAttendee.paperTitle != null)
            {
                //If presenter is true, then display presenter certificate.
                if (objAttendee.presenter)
                    lblCertificate.Text = "This is to certify that " + objAttendee.firstName + " " + objAttendee.lastName + " attended " + objAttendee.conferenceName + " and presented a paper entitled " + objAttendee.paperTitle;
                //If presenter is false, then display attendee certificate
            }
            else if (objAttendee.firstName != null && objAttendee.lastName != null && objAttendee.conferenceName != null)
            {
                if (!(objAttendee.presenter))
                    lblCertificate.Text = "This is to certify that " + objAttendee.firstName + " " + objAttendee.lastName + " attended " + objAttendee.conferenceName + ".";
            }
            //If one of the components of the certificate is null, warn the user that the certificate cannot be displayed until this has been changed
            else
                lblCertificate.Text = "Please ensure that all appropriate boxes have been \n filled before trying to disply the certificate.";
        }
    }
}
